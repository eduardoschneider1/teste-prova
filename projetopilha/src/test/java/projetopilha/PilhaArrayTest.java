package projetopilha;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import br.edu.iftm.ads.projetopilha.Pilha;
import br.edu.iftm.ads.projetopilha.PilhaLDL;
import br.edu.iftm.ads.projetopilha.excecoes.EstruturaVaziaException;

public class PilhaArrayTest  {

	@Test 
	public void devePermitirInserirUmElementoNaPilha() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
	
		 pilha.push(10);
		 
		 assertEquals(1, pilha.getTamanho(), "Deveria ter um elemento");
		 assertTrue(pilha.getTamanho() == 1);
		 
		 assertEquals(10, pilha.topo());
	}
	
	//os do exercicio abaixo:
	
	
	//inserir mais de um elemento
	
	@Test 
	public void devePermitirInserirMaisDeUmElementoNaPilha() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 assertTrue(pilha.getTamanho() > 0);
		 pilha.push(10);
		 
		 assertTrue(pilha.topo() == 10);
	}
	
	//ordem dos elementos inseridos
	
	@Test 
	public void deveInserirElementosEmOrdemDePilha() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 pilha.push(10);
		 assertTrue(pilha.topo() == 10);
		 pilha.push(250);
		 assertTrue(pilha.topo() == 250);
		 pilha.push(1);
		 assertTrue(pilha.topo() == 1);
	}
	
	//remover de uma pilha vazia
	
	@Test 
	public void devePermitirRemoverDeUmaPilhaVazia() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 assertTrue(pilha.isVazia());
		 assertThrows(EstruturaVaziaException.class, () -> pilha.pop());
	}
	
	//remover elemento pilha com 1 elemmento
	
	@Test 
	public void devePermitirRemover1PilhaCom1() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 pilha.push(10);
		 assertTrue(pilha.getTamanho() == 1);
		 pilha.pop();
		 assertTrue(pilha.getTamanho() == 0);
		 
	}
	
	//remover elemento pilha com + de 1 elemmento
	
	@Test 
	public void devePermitirRemover1PilhaComMaisDe1() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 pilha.push(10);
		 pilha.push(20);
		 pilha.push(30);
		 pilha.push(40);
		 pilha.push(50);
		 
		 assertTrue(pilha.getTamanho() == 5);
		 pilha.pop();
		 assertTrue(pilha.getTamanho() == 4);
	}
	
	//topo pilha vazia
	
	@Test 
	public void devePermitirTopoPilhaVazia() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 assertTrue(pilha.isVazia());
		 assertNull(pilha.topo());
	}
	
	//topo pilha 1 elemento
	
	@Test 
	public void devePermitirTopoPilha1Elemento() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 pilha.push(10);
		 assertTrue(pilha.getTamanho() == 1);
		 assertTrue(pilha.topo() == 10);
		 
	}
	
	//topo pilha + de 1 elemento
	
	@Test 
	public void devePermitirTopoPilhaMaisDe1Elemento() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 pilha.push(10);
		 pilha.push(20);
		 pilha.push(30);
		 pilha.push(40);
		 pilha.push(50);
		 
		 assertTrue(pilha.getTamanho() == 5);
		 assertTrue(pilha.topo() == 50);
		 
	}
	
	//tamanho pilha vazia
	
	@Test 
	public void devePermitirTamanhoPilhaVazia() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 assertTrue(pilha.isVazia());
		 assertTrue(pilha.getTamanho() == 0);
	}
	
	//tamanho pilha 1 elemento
	
	@Test 
	public void devePermitirTamanhoPilha1Elemento() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 pilha.push(10);
		 assertTrue(pilha.getTamanho() == 1);
		 
	}
	
	//tamanho pilha + de 1 elemento
	
	@Test 
	public void devePermitirTamanhoPilhaMaisDe1Elemento() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 pilha.push(10);
		 pilha.push(20);
		 pilha.push(30);
		 pilha.push(40);
		 pilha.push(50);
		 
		 assertTrue(pilha.getTamanho() == 5);
		 
	}
	
	//limpar pilha vazia
	
	@Test 
	public void devePermitirLimparPilhaVazia() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 assertTrue(pilha.isVazia());
		 assertTrue(pilha.getTamanho() == 0);
		 pilha.limpar();
		 assertTrue(pilha.isVazia());
		 assertTrue(pilha.getTamanho() == 0);

	}
	
	//limpar pilha com 1 elemento
	
	@Test 
	public void devePermitirLimparPilha1() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 pilha.push(10);
		 assertTrue(pilha.getTamanho() == 1);
		 pilha.limpar();
		 assertTrue(pilha.isVazia());
		 assertTrue(pilha.getTamanho() == 0);
	}
	
	//limpar pilha com + de 1
	
	@Test 
	public void devePermitirLimparPilhaMaisDe1() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 pilha.push(10);
		 pilha.push(20);
		 pilha.push(30);
		 pilha.push(40);
		 pilha.push(50);
		 assertTrue(pilha.getTamanho() == 5);
		 pilha.limpar();
		 assertTrue(pilha.isVazia());
		 assertTrue(pilha.getTamanho() == 0);
	}
	
	//pilha vazia esta vazia
	
	@Test 
	public void devePermitirPilhaVaziaEstaVazia() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 assertTrue(pilha.getTamanho() == 0);
		 assertTrue(pilha.isVazia());
	}
	
	//pilha nao vazia esta vazia
	
	@Test 
	public void devePermitirPilhaNaoVaziaEstaVazia() {
		 Pilha<Integer> pilha = new PilhaLDL<>();
		 pilha.push(10);
		 assertTrue(pilha.getTamanho() == 1);
		 assertFalse(pilha.isVazia());
	}
	
}
