package br.edu.iftm.ads.projetopilha.excecoes;

public class EstruturaVaziaException extends ED2Exception {

	private static final long serialVersionUID = -1756989086727412382L;

	public EstruturaVaziaException(String mensagem) {
        super(mensagem);
    }
}
