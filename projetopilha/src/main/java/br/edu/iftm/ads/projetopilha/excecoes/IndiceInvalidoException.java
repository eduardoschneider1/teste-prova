package br.edu.iftm.ads.projetopilha.excecoes;

public class IndiceInvalidoException extends ED2Exception {

	private static final long serialVersionUID = 8078770158849062012L;

	public IndiceInvalidoException(String mensagem) {
        super(mensagem);
    }
}
