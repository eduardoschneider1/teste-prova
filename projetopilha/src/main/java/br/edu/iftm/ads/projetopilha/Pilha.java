package br.edu.iftm.ads.projetopilha;

import br.edu.iftm.ads.projetopilha.excecoes.EstruturaVaziaException;

public interface Pilha<T extends Comparable<T>> {

	void push(T valor);

	T pop() throws EstruturaVaziaException;

	T topo() throws EstruturaVaziaException;

	int getTamanho();

	void limpar();

	boolean isVazia();

	String toString();

}