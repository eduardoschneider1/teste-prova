package br.edu.iftm.ads.projetopilha.excecoes;

public class ED2Exception extends RuntimeException {

	private static final long serialVersionUID = -2702161238927228679L;

	public ED2Exception(String mensagem) {
        super(mensagem);
    }
}
