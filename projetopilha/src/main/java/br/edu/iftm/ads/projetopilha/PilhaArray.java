package br.edu.iftm.ads.projetopilha;

import java.util.Arrays;

import br.edu.iftm.ads.projetopilha.excecoes.EstruturaVaziaException;

public class PilhaArray<T extends Comparable<T>> implements Pilha<T> {

	private int tamanho = 0;
	private static final int CAPACIDADE_DEFAULT = 10;
	private Object elementos[];

	public PilhaArray() {
		elementos = new Object[CAPACIDADE_DEFAULT];
	}

	@Override
	public void push(T valor) {
		if (tamanho == elementos.length) {
			aumentarCapacidade();
		}
		elementos[tamanho++] = valor;
	}

	private void aumentarCapacidade() {
		int novoTamanho = elementos.length * 2;
		elementos = Arrays.copyOf(elementos, novoTamanho);
	}

	@Override
	public T pop() throws EstruturaVaziaException {
		if (tamanho == 0) {
			throw new EstruturaVaziaException("Impossível fazer um pop em uma pilha vazia");
		}
		@SuppressWarnings("unchecked")
		T e = (T) elementos[--tamanho];
		elementos[tamanho] = null;
		return e;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T topo() throws EstruturaVaziaException {
		if (tamanho == 0) {
			throw new EstruturaVaziaException("Impossível obter o topo de uma pilha vazia");
		}
		return (T) elementos[tamanho];
	}

	@Override
	public int getTamanho() {
		return tamanho;
	}

	@Override
	public void limpar() {
		tamanho = 0;
		elementos = new Object[CAPACIDADE_DEFAULT];
	}

	@Override
	public boolean isVazia() {
		return tamanho == 0;
	}

	@Override
	public String toString() {
		String tudo = "";
		if (tamanho != 0) {
			int cont;
			for (cont = 0; cont < tamanho - 1; cont++) {
				tudo += elementos[cont];
				tudo += " " + "<=";
			}
		} else {
			tudo = "Estrutura vazia.";
		}
		return tudo;
	}

}
