package br.edu.iftm.ads.projetopilha;

import br.edu.iftm.ads.projetopilha.excecoes.EstruturaVaziaException;
import br.edu.iftm.ads.projetopilha.excecoes.IndiceInvalidoException;

public class ListaDuplamenteLigada<T extends Comparable<T>> {

    private NoLDL<T> inicio, fim;
    private int tamanho;

    public ListaDuplamenteLigada() {
        inicio = fim = null;
        tamanho = 0;
    }

    public void insereInicio(T valor) {
        if (tamanho == 0) {  //Lista Vazia
            inicio = fim = new NoLDL<T>(valor);
        } else {
            inicio = new NoLDL<T>(valor, inicio, null);
            inicio.proximo.anterior = inicio;
        }
        tamanho++;
    }

    public void insereFim(T valor) {
        if (tamanho == 0) {  //Lista Vazia
            inicio = fim = new NoLDL<T>(valor);
        } else {
            fim = new NoLDL<T>(valor, null, fim);
            fim.anterior.proximo = fim;
        }
        tamanho++;
    }

    public void inserePosicao(T valor, int posicao) throws IndiceInvalidoException {
        if ((posicao <= 0) || (posicao > tamanho - 1)) {
            throw new IndiceInvalidoException("Insercao em posicao invalida");
        } else {
            int cont = 0;
            NoLDL<T> aux = inicio;

            while (cont < posicao) {
                aux = aux.proximo;
                cont++;
            }

            aux.anterior.proximo = new NoLDL<T>(valor, aux, aux.anterior);
            aux.anterior = aux.anterior.proximo;
            tamanho++;
        }
    }

    public void removeInicio() throws EstruturaVaziaException {
        if (tamanho != 0) {
            if (tamanho == 1) {
                inicio = fim = null;
            } else {
                inicio = inicio.proximo;
                inicio.anterior = null;
            }
            tamanho--;
        } else {
            throw new EstruturaVaziaException("Impossivel remover inicio de uma lista vazia.");
        }
    }

    public void removeFim() throws EstruturaVaziaException {
        if (tamanho != 0) {
            if (tamanho == 1) {
                inicio = fim = null;
            } else {
                fim = fim.anterior;
                fim.proximo = null;
            }
            tamanho--;
        } else {
            throw new EstruturaVaziaException("Impossivel remover o fim de uma lista vazia.");
        }
    }

    public void removePosicao(int posicao) throws EstruturaVaziaException, IndiceInvalidoException {
        if (tamanho == 0) {
            throw new EstruturaVaziaException("Impossivel remover uma posicao de uma lista vazia.");
        } else if ((posicao <= 0) || (posicao > tamanho - 1)) {
            throw new IndiceInvalidoException("Remocao em posicao invalida.");
        } else {
            int cont = 0;
            NoLDL<T> aux = inicio;

            while (cont < posicao) {
                aux = aux.proximo;
                cont++;
            }

            aux.anterior.proximo = aux.proximo;
            aux.proximo.anterior = aux.anterior;
            tamanho--;
        }
    }

    public T getInfoInicio() throws EstruturaVaziaException {
        if (tamanho != 0) {
            return inicio.valor;
        } else {
            throw new EstruturaVaziaException("Impossivel retornar o valor do inicio de uma lista vazia.");
        }
    }

    public T getInfoFim() throws EstruturaVaziaException {
        if (tamanho != 0) {
            return fim.valor;
        } else {
            throw new EstruturaVaziaException("Impossivel retornar o valor do fim de uma lista vazia.");
        }
    }

    public T getInfoPosicao(int posicao) throws IndiceInvalidoException,
            EstruturaVaziaException {
        if (tamanho != 0) {
            if ((posicao <= 0) || (posicao > tamanho - 1)) {
                throw new IndiceInvalidoException("Impossivel retornar o valor de uma posicao invalida.");
            } else {
                int cont = 0;
                NoLDL<T> aux = inicio;

                while (cont < posicao) {
                    aux = aux.proximo;
                    cont++;
                }

                return aux.valor;
            }
        } else {
            throw new EstruturaVaziaException("Impossivel retornar o valor de uma posicao de uma lista vazia.");
        }
    }

    public boolean existeInfo(T valor) {
        return getPosicaoInfo(valor) != -1;
    }

    public int getPosicaoInfo(T valor) {
        if (tamanho != 0) {
            int cont = 0;
            NoLDL<T> aux = inicio;

            while ((aux != null) && (!aux.valor.equals(valor))) {
                aux = aux.proximo;
                cont++;
            }
            if (cont != tamanho) {
                return cont;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    public int getTamanho() {
        return tamanho;
    }

    public void limpar() {
        inicio = fim = null;
        tamanho = 0;
    }

    public boolean isVazia() {
        return tamanho == 0;
    }

    public void mostrar() {
        if (tamanho != 0) {
            NoLDL<T> aux = inicio;

            while (aux.proximo != null) {
                System.out.print(aux.valor);
                System.out.print("=>");
                aux = aux.proximo;
            }
            System.out.println(aux.valor);
        } else {
            System.out.println("Estrutura vazia.");
        }
    }

    private class NoLDL<P> {

        public P valor;
        public NoLDL<P> proximo, anterior;

        @SuppressWarnings("unused")
		public NoLDL() {
            proximo = anterior = null;
        }

        public NoLDL(P valor) {
            this.valor = valor;
            proximo = anterior = null;
        }

        public NoLDL(P valor, NoLDL<P> proximo, NoLDL<P> anterior) {
            this.valor = valor;
            this.proximo = proximo;
            this.anterior = anterior;
        }
    }
}
