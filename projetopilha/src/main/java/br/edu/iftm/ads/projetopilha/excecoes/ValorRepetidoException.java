package br.edu.iftm.ads.projetopilha.excecoes;

public class ValorRepetidoException extends ED2Exception {

	private static final long serialVersionUID = -1307091903371031364L;

	public ValorRepetidoException(String mensagem) {
        super(mensagem);
    }
}
